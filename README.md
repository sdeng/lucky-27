# Lucky 27

## Prerequisits
* Adobe Flex SDK 4.6
* Adobe Air SDK 3.8
* Apache Ant

## Build
* Clone repository
* Create my.build.properties with the following values
    + flex.dir=<path-to-flex-sdk>
    + air.dir=<path-to-air-sdk>
* Run "ant" to build and launch
