package com.fancy_lads_academy.lucky_27
{
    import starling.display.Sprite;
    import starling.events.Event;

    import com.fancy_lads_academy.lucky_27.scenes.Menu;
    import com.fancy_lads_academy.lucky_27.scenes.StartBand;
    import com.fancy_lads_academy.lucky_27.utils.NavigationEvent


    public class Lucky_27 extends Sprite
    {
        public function Lucky_27()
        {
            super();

            // Triggered when the Starling is added to stage
            this.addEventListener(Event.ADDED_TO_STAGE, addedToStage);
        }

        private var menu:Menu;
        private var startBand:StartBand;

        private function addedToStage(event:Event):void
        {
            // Initialization should be called only once
            this.removeEventListener(Event.ADDED_TO_STAGE, addedToStage);

            this.addEventListener(NavigationEvent.CHANGE_SCENE, changeScene);

            startBand = new StartBand();
            startBand.hide();
            this.addChild(startBand);

            menu = new Menu();
            this.addChild(menu);
            menu.show();
        }

        private function changeScene(event:NavigationEvent):void
        {
            switch (event.params.id)
            {
                case 'newBand':
                    menu.hide();
                    startBand.show();
                    break;
            }
        }
    }
}
