package com.fancy_lads_academy.lucky_27
{
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode

    import starling.core.Starling;
    import starling.utils.Color;

    import com.fancy_lads_academy.lucky_27.Lucky_27;


    [SWF(backgroundColor="#000000")]
    public class Bootstrap extends Sprite
    {
        private var _starling:Starling;

        public function Bootstrap()
        {
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;

            _starling = new Starling(Lucky_27, stage);
            _starling.start();
        }
    }
}
