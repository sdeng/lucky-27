package com.fancy_lads_academy.lucky_27.utils
{
    import starling.events.Event;


    public class NavigationEvent extends Event
    {
        public function NavigationEvent(type:String, _params:Object=null, bubbles:Boolean=false)
        {
            super(type, bubbles);
            this.params = _params;
        }

        public static const CHANGE_SCENE:String = 'changeScreen';
        public var params:Object;
    }
}
