package com.fancy_lads_academy.lucky_27.utils
{
    import flash.display.Bitmap;
    import flash.utils.Dictionary;

    import starling.textures.Texture;

    public class Assets
    {
        [Embed(source='../assets/images/menu/background.jpg')]
        public static const MenuBackground:Class;

        public static function getTexture(name:String):Texture
        {
            if (gameTextures[name] == undefined)
            {
                var bitmap:Bitmap = new Assets[name]();
                gameTextures[name] = Texture.fromBitmap(bitmap);
            }

            return gameTextures[name];
        }

        private static var gameTextures:Dictionary = new Dictionary();
    }
}
