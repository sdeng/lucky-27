package com.fancy_lads_academy.lucky_27.scenes
{
    import feathers.controls.Button;
    import feathers.themes.MinimalMobileTheme;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;

    import com.fancy_lads_academy.lucky_27.utils.Assets
    import com.fancy_lads_academy.lucky_27.utils.NavigationEvent


    public class Menu extends Sprite
    {
        public function Menu()
        {
            super();

            // Triggered when this scene is added to stage
            this.addEventListener(Event.ADDED_TO_STAGE, addedToStage);
        }

        public function show():void
        {
            this.visible = true;
        }

        public function hide():void
        {
            this.visible = false;
        }

        private var background:Image;
        private var newGame:Button;

        private function addedToStage(event:Event):void
        {
            renderBackground();
            renderButtons();
        }

        private function renderBackground():void
        {
            background = new Image(Assets.getTexture('MenuBackground'));
            this.addChild(background);
        }

        private function renderButtons():void
        {
            new MinimalMobileTheme();

            this.newGame = new Button();
            this.newGame.label = 'Start a band!';
            this.addChild(this.newGame);

            // Calculate button's dimensions immediately so we can position it
			this.newGame.validate();
			this.newGame.x = (this.stage.stageWidth - this.newGame.width) / 2;
			this.newGame.y = (this.stage.stageHeight - this.newGame.height) / 2;

            this.newGame.addEventListener(Event.TRIGGERED, buttonClick);
        }

		private function buttonClick(event:Event):void
		{
            this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCENE, {id: 'newBand'}, true));
		}
    }
}
