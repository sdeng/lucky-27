package com.fancy_lads_academy.lucky_27.scenes
{
    import feathers.controls.Button;
    import feathers.controls.Callout;
    import feathers.controls.Label;
    import feathers.themes.MinimalMobileTheme;
    import starling.display.Sprite;
    import starling.events.Event;

    import com.fancy_lads_academy.lucky_27.utils.Assets
    import com.fancy_lads_academy.lucky_27.utils.NavigationEvent


    public class StartBand extends Sprite
    {
        public function StartBand()
        {
            super();

            // Triggered when this scene is added to stage
            this.addEventListener(Event.ADDED_TO_STAGE, addedToStage);
        }

        public function show():void
        {
            this.visible = true;
        }

        public function hide():void
        {
            this.visible = false;
        }

        private var punk:Button;
        private var emo:Button;
        private var metal:Button;
        private var rockabilly:Button;
        private var newWave:Button;

        private function addedToStage(event:Event):void
        {
            renderGenres();
        }

        private function renderGenres():void
        {
            new MinimalMobileTheme();

            this.punk = new Button();
            this.punk.label = 'Punk';
            this.addChild(this.punk);
            this.punk.validate();
			this.punk.x = (this.stage.stageWidth - this.punk.width) / 2;
			this.punk.y = 100;
            this.punk.addEventListener(Event.TRIGGERED, pickGenre);

            this.emo = new Button();
            this.emo.label = 'Emo';
            this.addChild(this.emo);
            this.emo.validate();
			this.emo.x = (this.stage.stageWidth - this.emo.width) / 2;
			this.emo.y = 200;
            this.emo.addEventListener(Event.TRIGGERED, pickGenre);

            this.metal = new Button();
            this.metal.label = 'Metal';
            this.addChild(this.metal);
            this.metal.validate();
			this.metal.x = (this.stage.stageWidth - this.metal.width) / 2;
			this.metal.y = 300;
            this.metal.addEventListener(Event.TRIGGERED, pickGenre);

            this.rockabilly = new Button();
            this.rockabilly.label = 'Rockabilly';
            this.addChild(this.rockabilly);
            this.rockabilly.validate();
			this.rockabilly.x = (this.stage.stageWidth - this.rockabilly.width) / 2;
			this.rockabilly.y = 400;
            this.rockabilly.addEventListener(Event.TRIGGERED, pickGenre);

            this.newWave = new Button();
            this.newWave.label = 'New Wave';
            this.addChild(this.newWave);
            this.newWave.validate();
			this.newWave.x = (this.stage.stageWidth - this.newWave.width) / 2;
			this.newWave.y = 500;
            this.newWave.addEventListener(Event.TRIGGERED, pickGenre);
        }

        private function pickGenre(event:Event):void
        {
            switch (event.target as Button)
            {
                case this.punk:
                    describeGenre(this.punk, 'Loud, fast playing rocker who hates anything mainstream.');
                    break;
                case this.emo:
                    describeGenre(this.emo, 'The pain of his/her existence flows through their music.');
                    break;
                case this.metal:
                    describeGenre(this.metal, 'Tough as the music he plays.');
                    break;
                case this.rockabilly:
                    describeGenre(this.rockabilly, 'Sweet licks and swinging beats make this guys/girls style irresistible.');
                    break;
                case this.newWave:
                    describeGenre(this.newWave, 'Experimental subculture of electronic instruments.');
                    break;
            }
        }

        private function describeGenre(genreButton:Button, description:String):void
        {
            const label:Label = new Label();
            label.text = description;
            Callout.show(label, genreButton);
        }
    }
}
